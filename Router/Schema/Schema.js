const mongoose = require("mongoose")

const billingschema = new mongoose.Schema({
    name :String,
    productname: String,
    email:String,
    id:String,
    pickupplace :String,
    deliveryplace:String,
    quantity : Number,
    quality: Number,
    date:String,
    phone:String
})
module.exports = mongoose.model("billing",billingschema)