const express = require ("express")
const billing = require("./Schema/Schema.js")

const Router = express.Router();


Router.post("/new",(req,res)=>{
    let payload = req.body;
try {
    const data = new billing(payload)

    data.save((err,data)=>{
     if(err){
         res.status(400).send("data is invalid")
         console.log(err)
     }else{
         res.status(200).send(data)
     }
    })
} catch (error) {
    console.log(error)
}
})

Router.get("/",(req,res)=>{
 try {
    billing.find((err,data)=>{
        if(err || ! data){
            res.status(400).send("data is not avalible")
            console.log(err)
        }else{
            res.status(200).send(data)
        }
})
 } catch (error) {
    console.log(error)
 }
}) 
Router.put("/edit",(req,res)=>{
try {
    const payload = req.body.id
    billing.findOneAndUpdate({id:payload },{$set: req.body}, {new: true},(err,data)=>{
        if(err || ! data){
            res.status(400).send("invaild data")
            console.log(err)
        }else{
            res.status(200).send(data)
        }
})
} catch (error) {
    console.log(error)
}
}) 

Router.get("/get/:id",(req,res)=>{
 try {
    const payload = req.params.id
    billing.find({id : payload},(err,data)=>{
        if(err || !data){
           console.log(err)
            res.status(400).json(err,"this data is invalid")
        }

        res.status(200).send(data)
    })
 } catch (error) {
    
 }
})


Router.get("/:id",(req,res)=>{
  try {
    const payload = req.params.id
    console.log(payload)
     billing.findOneAndDelete({id:payload},(err,data)=>{
        if(err|| !data){
            res.status(400).send("not deleeted")
        }
        res.status(200).send("data deleted successfully....")
        console.log("data deleted successfully....",data)
     })
  } catch (error) {
    console.log(error)
  }
})
module.exports = Router;
