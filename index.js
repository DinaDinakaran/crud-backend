const express = require("express")
const connection = require("./Db connection/connection");
require("dotenv").config();
const Router = require("./Router/Router");
const app = express();
connection();

let port = process.env.PORT
app.use(express.json());
app.use("/api",Router)

app.listen(port,()=>console.log(`server run on ${port}`))