const mongoose = require("mongoose")
require("dotenv").config();
const db= process.env.URL
console.log(db)
 const connection= ()=>{

    mongoose.connect(process.env.URL,{
      useNewUrlParser: true,
      useUnifiedTopology: true   })   
.then(() => console.log("Database connected!"))
.catch(err => console.log(err));

}

module.exports = connection;
